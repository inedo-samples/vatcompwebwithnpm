﻿using Microsoft.AspNetCore.Mvc;

namespace VatCompWebWithNpm.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VatCompController : ControllerBase
    {
        private readonly ILogger<VatCompController> _logger;

        public VatCompController(ILogger<VatCompController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public decimal Post([FromBody] VatComputerInput model)
        {
            return model.sales * model.vat;
        }
    }

    public record VatComputerInput(decimal sales, decimal vat);
}

import React, { Component, useState } from 'react';
import $ from 'jquery';
import { NumberFormatBase } from 'react-number-format';

function MyCustomNumberFormat(props) {
    const format = (numStr) => {
        if (numStr === "") return "";
        return new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
            maximumFractionDigits: 0
        }).format(numStr);
    };

    return <NumberFormatBase {...props} format={format} />;
}
export class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);
        this.state = { result: 0.00 };
        this.calculateVat = this.calculateVat.bind(this);
    }

    

    render() {
        return (
            <div>
                <h1>Welcome to VAT Computer 1000</h1>
                <div className="form-fields">
                    <div>
                        <label>Sale Amount:</label><br/>
                        <input id="sales" type="number" />
                    </div>
                    <div>
                        <label>VAT Amount:</label><br/>
                        <input id="vat" type="number" />
                    </div>
                </div>
                <div>
                    <div className="button-container">
                        <button id="compute-btn" onClick={this.calculateVat} className="btn btn-primary">Compute</button>
                    </div>
                    <div className="results">
                        <label>VAT Due:</label>
                        <strong>
                            <MyCustomNumberFormat value={this.state.result} displayType={'text'} />
                        </strong>
                    </div>
                </div>
            </div>
        );
    }

    async calculateVat() {

        let bodyObj = {
            'sales': $('#sales').val(),
            'vat': $('#vat').val()
        };
        console.log(bodyObj);
        await fetch('/vatcomp', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(bodyObj)
        })
        .then(response => response.json())
        .then(data => {
            this.setState({ result: data });
        })
        .catch(error => {
            // Handle any errors
            console.error('Error:', error);
        });
    }

    
}
